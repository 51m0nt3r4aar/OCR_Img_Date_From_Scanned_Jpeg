# potential libraries for ocr for this project
import ocropus #google tool, trainable
import pyteseract #older google tool, also trainable
import ocrad # gnu OCR

# concerning the writing of the exif data
import PIL #python image lib, forked
import piexif #tool for reading and writing exif

# image manipulation and reduction and color detection
from tensorflow import image
import PIL # pip3 install Pillow
import cv2 #openCV in python, powerful, but maybe to complex for the needs here (color matching and bounding box)
import skimage #scikit image
import 
