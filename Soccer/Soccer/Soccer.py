# Soccer is a python3 wrapper for ssocr (Seven Segment Optical Character Recognition) tool
# Segmented Optical Character Chained Environment Relations
#


import sys
import subprocess
import numpy

class Soccer:

    def __init__(self):
        self.program_name = "ssocr"
        self.min_version_number = 0

    def installation_check(self):
        return
        # check if ssocr is installed

    def build_arguments(self, args):
        return

    def parse_returned_values(self, values):
        # parse results to a python container
        return

    def coordinates_to_tuple(self):
        # parse
        return

    def manip_stdin(self):
        return

    def run_soccer(self, np_img: numpy.ndarray, exp_char: int, coord: bool, ):
        """
        takes the params to run the ssocr command

        :param np_img: numpy array containing the image to be
        :param exp_char:
        :param coord:
        :return:
        """
        return
